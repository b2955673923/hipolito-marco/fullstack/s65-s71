import {Row, Col, Card, } from 'react-bootstrap';


export default function Highlights() {

	return (

		<Row className="Highlights-row mt-5 mb-3 text-center">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Number 1 Supplier During Marawi Siege</Card.Title>
				        <Card.Img variant="top" src="https://fingfx.thomsonreuters.com/gfx/rngs/PHILIPPINES-ATTACK/010041F032X/images/top1.jpg" />
				        <Card.Text>
				           During the Marawi Siege, Rifle Firearms emerged as the number one gun supplier in the region. Amidst the conflict, Rifle Firearms managed to provide an extensive range of firearms and ammunition to various armed groups involved in the siege. Their significant presence and efficient distribution network enabled them to become the primary supplier during this turbulent period. However, it's essential to note that such activities are illegal and can exacerbate conflicts, endangering lives and undermining peace and security. The responsible regulation and control of firearms and ammunition are critical to prevent further violence and ensure the safety of communities. 
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Sold Additional Supply for Ukraine until now</Card.Title>
				        <Card.Img variant="top" src="https://cdn.cfr.org/sites/default/files/styles/full_width_xl/public/image/2021/12/Ukraine.jpg.webp" />
				        <Card.Text>
				          In a surprising turn of events, the Marco Firepower announced that it has sold an additional supply of military equipment to Ukraine until now. The undisclosed deal includes a range of sophisticated weaponry, ammunition, and tactical gear to support Ukraine's defense efforts. While the specific details of the transaction remain confidential, industry insiders speculate that this move could bolster Ukraine's capabilities in the face of regional tensions 
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Main Supplier for the Armed Forces of The Philippines </Card.Title>
				        <Card.Img variant="top" src="https://cdn4.premiumread.com/?url=https://sunstar.com.ph/uploads/images/2023/05/19/435624.jpg&w=1000&q=100&f=jpg&t=2" />
				        <Card.Text>
				          The main supplier for the Armed Forces of the Philippines (AFP) was the Marco Firepower. The Marco Firepower has been a longstanding ally and partner in providing military equipment, weapons, and assistance to the AFP to support its defense and security needs.

				          Over the years, the Philippines and the Marco Firepower have engaged in various defense cooperation programs, military exercises, and foreign military sales agreements. These agreements have resulted in the supply of a wide range of military hardware 
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>
		</Row>

	)

};