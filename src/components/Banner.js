import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner() {
	
	return (
		<section id="banner-image">
                <div className="banner-bg">
                    <div className="banner-container">
                        <h1 className="banner-title">Arm Yourself with Confidence: Your Trusted Gun Shop for Quality Firearms and Accessories!</h1>
                        <h2 className="banner-subtitle">
                            Our One-Stop Destination for Firearm Expertise, Safety Education, and Unbeatable Selection.
                        </h2>
                        <Link to="/products">
                        	<Button className="banner-apply-button">Buy now!</Button>
                        </Link>
                    </div>
                </div> 
            </section>
			
		) 
};