import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';
// import Partners from '../components/Partners';




export default function Home() {

	return (
		<>
			<Banner/>
			<FeaturedProducts/>
			<Highlights/>
			
			
		</>
	)

};